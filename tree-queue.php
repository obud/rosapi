<?php

/**
 * Copyright (c) 2020 Ondřej Budín (https://ondrejbudin.cz)
 */

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use PEAR2\Net\RouterOS;

$util = new RouterOS\Util(
	$client = new RouterOS\Client('192.168.88.1', 'username', 'userpassword')
);
$util->setMenu('/queue tree');

while (TRUE) {
	foreach ($util->getAll() as $item) {
		$download = (string)$item->getProperty('rate');
		$packetRateDownload = (string)$item->getProperty('packet-rate');

		echo '
Queue: ' . $item->getProperty('name') . '
Speed: ' . $download . ' bps (' . round((int)$download/1024/1024, 2) . ' Mbps)
Packet rate: ' . $packetRateDownload . ' pps
';
	}

	echo "\n==================================================\n";
	sleep(1);
}

<?php

/**
 * Copyright (c) 2020 Ondřej Budín (https://ondrejbudin.cz)
 */

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use PEAR2\Net\RouterOS;

$util = new RouterOS\Util(
	$client = new RouterOS\Client('192.168.88.1', 'username', 'userpassword')
);
$util->setMenu('/queue simple');

while (TRUE) {
	foreach ($util->getAll() as $item) {
		[$upload, $download] = explode('/', (string)$item->getProperty('rate'));
		[$packetRateUpload, $packetRateDownload] = explode('/', (string)$item->getProperty('packet-rate'));

		echo '
Queue: ' . $item->getProperty('name') . '
Speed:
  download: ' . $download . ' bps (' . round((int)$download/1024/1024, 2) . ' Mbps)
  upload: ' . $upload . ' bps (' . round((int)$upload/1024/1024, 2) . ' Mbps)
Packet rate:
  download: ' . $packetRateDownload . ' pps
  upload: ' . $packetRateUpload . ' pps
';
	}

	echo "\n==================================================\n";
	sleep(1);
}

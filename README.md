# MikroTik RouterOS API - queue stats

## Requirements
- PHP 7.3
- Composer (https://getcomposer.org/)

## Installation
```
$ composer install 
```

## Run 
```
$ php simple-queue.php 
```
```
$ php tree-queue.php 
```